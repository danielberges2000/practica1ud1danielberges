package com.danielberges.practica.gui;

import com.danielberges.practica.base.Menu;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Clase Principal
 * By Daniel Berges 08/11/21
 */
public class Ventana {

    public JPanel panel;
    public JButton bCerrar;
    public JTextField tfNumeroPedido;
    public JRadioButton rButtonHappyMeal;
    public JRadioButton rButtonMcAitana;
    public JComboBox cbHamburguesa;
    public JComboBox cbPatatas;
    public JComboBox cbBebida;
    public JComboBox cbJugueteComplemento;
    public JButton bImportar;
    public JButton bExportar;
    public JLabel jlFechaPedido;
    public JLabel jlNumeroPedido;
    public JLabel jlMenu;
    public JLabel jlHamburguesa;
    public JLabel jlPatatas;
    public JLabel jlBebidas;
    public JLabel jlJugueteComplemento;
    public JLabel jlVisualizacion;
    public DatePicker pDate;
    public JButton bNuevo;
    public JList lista;
    public javax.swing.JScrollPane JScrollPane;
    public JLabel jlDetalles;
    public JTextArea taDetalles;
    public JFrame frame;
    public DefaultListModel<Menu> dListModelMenu;

    /**
     * Constructor de la clase Ventana
     * LLamo al método iniciarComponentes
     */
    public Ventana() {

        frame = new JFrame("MenusMVC");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        iniciarComponentes();

    }

    /**
     * Método que inicia los componentes
     */
    private void iniciarComponentes() {

        dListModelMenu = new DefaultListModel<Menu>();
        lista.setModel(dListModelMenu);

    }

}
