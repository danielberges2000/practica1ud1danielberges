package com.danielberges.practica.gui;

import com.danielberges.practica.base.HappyMeal;
import com.danielberges.practica.base.McAitna;
import com.danielberges.practica.base.Menu;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase MenusModelo
 * By Daniel Berges 08/11/21
 */
public class MenusModelo {

    private ArrayList<Menu> listaMenus;

    /**
     * Constructor de la clase MenusModelo
     */
    public MenusModelo() {
        this.listaMenus = new ArrayList<Menu>();
    }

    public ArrayList<Menu> obtenerMenus() {
        return listaMenus;
    }

    /**
     * Método que alta un HappyMeal
     * @param fechaPedido La fecha del pedido
     * @param numeroPedido El numero del pedido
     * @param hamburguesa El tipo de hamburguesa
     * @param patatas El tipo de patatas
     * @param bebida El tipo de bebida
     * @param detalles Los detalles del pedido
     * @param juguete El tipo de juguete
     */
    public void altaHappyMeal(LocalDate fechaPedido, int numeroPedido, String hamburguesa, String patatas, String bebida, String detalles, String juguete) {
        HappyMeal nuevoHappyMeal = new HappyMeal(fechaPedido, numeroPedido, hamburguesa, patatas, bebida, detalles, juguete);
        listaMenus.add(nuevoHappyMeal);
    }

    /**
     * Método que alta un McAitana
     * @param fechaPedido La fecha del pedido
     * @param numeroPedido El numero del pedido
     * @param hamburguesa El tipo de hamburguesa
     * @param patatas El tipo de patatas
     * @param bebida El tipo de bebida
     * @param detalles Los detalles del pedido
     * @param complemento El tipo de complemento
     */
    public void altaMcAitana(LocalDate fechaPedido, int numeroPedido, String hamburguesa, String patatas, String bebida, String detalles, String complemento) {
        McAitna nuevoMcAitana = new McAitna(fechaPedido, numeroPedido, hamburguesa, patatas, bebida, detalles, complemento);
        listaMenus.add(nuevoMcAitana);
    }

    /**
     * Método que comprueba si exsite el numero del pedido
     * @param numeroPedido El numero del pedido
     * @return boolean
     */
    public boolean existeNumeroPedido(int numeroPedido) {
        for (Menu unMenu : listaMenus) {
            if (unMenu.getNumeroPedido() == numeroPedido) {
                return true;
            }
        }
        return false;
    }

    /**
     * Métdodo que exporta un fichero XML
     * @param fichero File
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Menus");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoMenu = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Menu unMenu : listaMenus) {

            if (unMenu instanceof HappyMeal) {
                nodoMenu = documento.createElement("HappyMeal");
            } else {
                nodoMenu = documento.createElement("McAitana");
            }

            raiz.appendChild(nodoMenu);

            nodoDatos=documento.createElement("fechaPedido");
            nodoMenu.appendChild(nodoDatos);
            texto=documento.createTextNode(unMenu.getFechaPedido().toString());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("numeroPedido");
            nodoMenu.appendChild(nodoDatos);
            texto=documento.createTextNode(String.valueOf(unMenu.getNumeroPedido()));
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("hamburguesa");
            nodoMenu.appendChild(nodoDatos);
            texto=documento.createTextNode(unMenu.getHamburguesa());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("patatas");
            nodoMenu.appendChild(nodoDatos);
            texto=documento.createTextNode(unMenu.getPatatas());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("bebida");
            nodoMenu.appendChild(nodoDatos);
            texto=documento.createTextNode(unMenu.getBebida());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("detalles");
            nodoMenu.appendChild(nodoDatos);
            texto=documento.createTextNode(unMenu.getDetalles());
            nodoDatos.appendChild(texto);

            if (unMenu instanceof McAitna) {
                nodoDatos=documento.createElement("complemento");
                nodoMenu.appendChild(nodoDatos);
                texto=documento.createTextNode(((McAitna) unMenu).getComplemento());
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos=documento.createElement("juguete");
                nodoMenu.appendChild(nodoDatos);
                texto=documento.createTextNode(((HappyMeal) unMenu).getJuguete());
                nodoDatos.appendChild(texto);
            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source,resultado);

        }
    }

    /**
     * Método que importa un fichero XML
     * @param fichero JFile
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {

        listaMenus = new ArrayList<Menu>();
        McAitna nuevoMcAitana = null;
        HappyMeal nuevoHappyMeal = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {

            Element nodoMenus = (Element) listaElementos.item(i);

            if (nodoMenus.getTagName().equals("McAitana")) {
                nuevoMcAitana = new McAitna();
                nuevoMcAitana.setFechaPedido(LocalDate.parse(nodoMenus.getChildNodes().item(0).getTextContent()));
                nuevoMcAitana.setNumeroPedido(Integer.parseInt(nodoMenus.getChildNodes().item(1).getTextContent()));
                nuevoMcAitana.setHamburguesa(nodoMenus.getChildNodes().item(2).getTextContent());
                nuevoMcAitana.setPatatas(nodoMenus.getChildNodes().item(3).getTextContent());
                nuevoMcAitana.setBebida(nodoMenus.getChildNodes().item(4).getTextContent());
                nuevoMcAitana.setDetalles(nodoMenus.getChildNodes().item(5).getTextContent());
                nuevoMcAitana.setComplemento(nodoMenus.getChildNodes().item(6).getTextContent());
                listaMenus.add(nuevoMcAitana);
            }else {
                if (nodoMenus.getTagName().equals("HappyMeal")) {
                    nuevoHappyMeal = new HappyMeal();
                    nuevoHappyMeal.setFechaPedido(LocalDate.parse(nodoMenus.getChildNodes().item(0).getTextContent()));
                    nuevoHappyMeal.setNumeroPedido(Integer.parseInt(nodoMenus.getChildNodes().item(1).getTextContent()));
                    nuevoHappyMeal.setHamburguesa(nodoMenus.getChildNodes().item(2).getTextContent());
                    nuevoHappyMeal.setPatatas(nodoMenus.getChildNodes().item(3).getTextContent());
                    nuevoHappyMeal.setBebida(nodoMenus.getChildNodes().item(4).getTextContent());
                    nuevoHappyMeal.setDetalles(nodoMenus.getChildNodes().item(5).getTextContent());
                    nuevoHappyMeal.setJuguete(nodoMenus.getChildNodes().item(6).getTextContent());
                    listaMenus.add(nuevoHappyMeal);
                }
            }
        }
    }
}
