package com.danielberges.practica.gui;

import com.danielberges.practica.base.HappyMeal;
import com.danielberges.practica.base.McAitna;
import com.danielberges.practica.base.Menu;
import com.danielberges.practica.util.Util;
import org.xml.sax.SAXException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Clase MenusControlador
 * By Daniel Berges 08/11/21
 */
public class MenusControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Ventana ventana;
    private MenusModelo modelo;
    private File ultimaRutaExportada;

    /**
     * Constructor de la clase MenusControlador
     * @param ventana Ventana
     * @param modelo Modelo
     */
    public MenusControlador(Ventana ventana, MenusModelo modelo) {

        this.ventana = ventana;
        this.modelo = modelo;
        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion" + e.getMessage());
        }
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * Método que carga los datos de la configuración
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {

        Properties configuracion = new Properties();
        configuracion.load(new FileReader("menus.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));

    }

    /**
     * Método que actualiza los datos de la configuración
     * @param ultimaRutaExportada File
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {

        this.ultimaRutaExportada = ultimaRutaExportada;

    }

    /**
     * Método que guarda los datos de la configuración
     * @throws IOException
     */
    private void guardarDatosConfiguracion() throws IOException {

        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("menus.conf"), "Datos configuracion menus");

    }

    /**
     * Método en el que añado las funcionalidades al programa mediante actionCommand
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String actionCommand = e.getActionCommand();
        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios:\n" + "Fecha Pedido\n" + "Nº Pedido");
                    break;
                }
                if (modelo.existeNumeroPedido(Integer.parseInt(ventana.tfNumeroPedido.getText()))) {
                    Util.mensajeError("Ya existe un menu con este numero de pedido: " + ventana.tfNumeroPedido.getText());
                    break;
                }
                if (ventana.rButtonHappyMeal.isSelected()) {
                    modelo.altaHappyMeal(ventana.pDate.getDate(),
                            Integer.parseInt(ventana.tfNumeroPedido.getText()),
                            ventana.cbHamburguesa.getSelectedItem().toString(),
                            ventana.cbPatatas.getSelectedItem().toString(),
                            ventana.cbBebida.getSelectedItem().toString(),
                            ventana.cbJugueteComplemento.getSelectedItem().toString(),
                            ventana.taDetalles.getText());
                } else {
                    modelo.altaMcAitana(ventana.pDate.getDate(),
                            Integer.parseInt(ventana.tfNumeroPedido.getText()),
                            ventana.cbHamburguesa.getSelectedItem().toString(),
                            ventana.cbPatatas.getSelectedItem().toString(),
                            ventana.cbBebida.getSelectedItem().toString(),
                            ventana.cbJugueteComplemento.getSelectedItem().toString(),
                            ventana.taDetalles.getText());
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar Recibo":
                JFileChooser selectorFichero1 = Util.crearSelectorFichero(ultimaRutaExportada, "Archivos XML","xml");
                int opt = selectorFichero1.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero1.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (SAXException e1) {
                        e1.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar Recibo":
                JFileChooser selectorFichero2 = Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (TransformerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;
            case "Happy Meal":
                ventana.jlJugueteComplemento.setText("Juguete");
                refrescarComboBoxJugetes();
                break;
            case "Mc Aitana":
                ventana.jlJugueteComplemento.setText("Complemento");
                refrescarComboBoxComplementos();
                break;
            case "Cerrar":
                int resp = Util.mensajeConfirmacion("¿Desea salir?", "Salir");
                if (resp == JOptionPane.OK_OPTION) {
                    try {
                        guardarDatosConfiguracion();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }
                break;
        }
    }

    /**
     * Método que refresca el ComboBox Complementos
     */
    private void refrescarComboBoxComplementos() {
        ArrayList<String> objetos = new ArrayList<>();
        objetos.add("Nuggets");
        objetos.add("McFlurry");
        ventana.cbJugueteComplemento.setModel(new DefaultComboBoxModel(objetos.toArray()));
    }


    /**
     * Método que refresca el ComboBox Juguetes
     */
    private void refrescarComboBoxJugetes() {
        ArrayList<String> objetos = new ArrayList<>();
        objetos.add("Chico");
        objetos.add("Chica");
        ventana.cbJugueteComplemento.setModel(new DefaultComboBoxModel(objetos.toArray()));
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Método en el que añado a los componentes de Ventana ActionListener
     * @param listener ActionListener
     */
    private void addActionListener(ActionListener listener) {
        ventana.bNuevo.addActionListener(listener);
        ventana.bCerrar.addActionListener(listener);
        ventana.bExportar.addActionListener(listener);
        ventana.bImportar.addActionListener(listener);
        ventana.rButtonHappyMeal.addActionListener(listener);
        ventana.rButtonMcAitana.addActionListener(listener);
        ventana.cbHamburguesa.addActionListener(listener);
        ventana.cbPatatas.addActionListener(listener);
        ventana.cbBebida.addActionListener(listener);
        ventana.cbJugueteComplemento.addActionListener(listener);
    }

    /**
     * Método en el que añado a frame WindowListener
     * @param listener WindowListener
     */
    private void addWindowListener(WindowListener listener) {
        ventana.frame.addWindowListener(listener);
    }

    /**
     * Método en el que añado a lista ListSelectionListener
     * @param listener ListSelectionListener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        ventana.lista.addListSelectionListener(listener);
    }

    /**
     * Método en el que limpio los campos
     */
    private void limpiarCampos() {
        ventana.pDate.setText(null);
        ventana.tfNumeroPedido.setText(null);
        ventana.cbHamburguesa.setSelectedIndex(0);
        ventana.cbPatatas.setSelectedIndex(0);
        ventana.cbBebida.setSelectedIndex(0);
        ventana.cbJugueteComplemento.setSelectedIndex(0);
        ventana.taDetalles.setText(null);
        ventana.tfNumeroPedido.requestFocus();
    }

    /**
     * Método en el que compruebo si hay campos vacios
     * @return boolean
     */
    private boolean hayCamposVacios() {
        if (ventana.pDate.getText().isEmpty() || ventana.tfNumeroPedido.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Método en el que refresto la lista
     */
    public void refrescar() {
        ventana.dListModelMenu.clear();
        for (Menu menu : modelo.obtenerMenus()) {
            ventana.dListModelMenu.addElement(menu);
        }
    }

    /**
     * Método para cerrar la ventana
     * @param e WindowEvent
     */
    @Override
    public void windowClosing(WindowEvent e) {

        int resp = Util.mensajeConfirmacion("¿Desea salir?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarDatosConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Método que ajusta los valores al seleccionar un objeto de la lista
     * @param e ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            Menu menuSeleccionado = (Menu) ventana.lista.getSelectedValue();
            ventana.pDate.setDate(menuSeleccionado.getFechaPedido());
            ventana.tfNumeroPedido.setText(String.valueOf(menuSeleccionado.getNumeroPedido()));
            seleccionarComboBoxHamburguesa(menuSeleccionado);
            seleccionarComboBoxPatatas(menuSeleccionado);
            seleccionarComboBoxBebida(menuSeleccionado);
            ventana.taDetalles.setText(menuSeleccionado.getDetalles());
            if (menuSeleccionado instanceof McAitna) {
                ventana.rButtonMcAitana.doClick();
                seleccionarComboBoxComplementos(menuSeleccionado);
            } else {
                ventana.rButtonHappyMeal.doClick();
                seleccionarComboBoxJuguetes(menuSeleccionado);
            }
        }
    }

    /**
     * Método para seleccionar el ComboBox de Hamburguesa
     * @param menuSeleccionado Menu
     */
    private void seleccionarComboBoxHamburguesa(Menu menuSeleccionado) {
        refrescarComboBoxComplementos();
        String hamburguesa = menuSeleccionado.getHamburguesa();
        if(hamburguesa.equals("Vacuno")) {
            ventana.cbHamburguesa.setSelectedIndex(0);
        }else {
            ventana.cbHamburguesa.setSelectedIndex(1);
        }
    }

    /**
     * Método para seleccionar el ComboBbox de Patatas
     * @param menuSeleccionado Menu
     */
    private void seleccionarComboBoxPatatas(Menu menuSeleccionado) {
        refrescarComboBoxComplementos();
        String patatas = menuSeleccionado.getHamburguesa();
        if(patatas.equals("Normales")) {
            ventana.cbPatatas.setSelectedIndex(0);
        } else{
            ventana.cbPatatas.setSelectedIndex(1);
        }
    }

    /**
     * Método para seleccionar el ComboBox de Bebida
     * @param menuSeleccionado Menu
     */
    private void seleccionarComboBoxBebida(Menu menuSeleccionado) {
        refrescarComboBoxComplementos();
        String bebida = menuSeleccionado.getBebida();
        if(bebida.equals("Agua")) {
            ventana.cbBebida.setSelectedIndex(0);
        }else if(bebida.equals("Coca Cola")) {
            ventana.cbBebida.setSelectedIndex(1);
        }else {
            ventana.cbBebida.setSelectedIndex(2);
        }
    }

    /**
     * Método para seleccionar el ComboBox de Complementos
     * @param menuSeleccionado Menu
     */
    private void seleccionarComboBoxComplementos(Menu menuSeleccionado) {
        refrescarComboBoxComplementos();
        String complemento = ((McAitna) menuSeleccionado).getComplemento();
        if(complemento.equals("Nuggets")) {
            ventana.cbJugueteComplemento.setSelectedIndex(0);
        }else {
            ventana.cbJugueteComplemento.setSelectedIndex(1);
        }
    }

    /**
     * Método para seleccionar el ComboBox de Juguetes
     * @param menuSeleccionado Menu
     */
    private void seleccionarComboBoxJuguetes(Menu menuSeleccionado) {
        refrescarComboBoxComplementos();
        String juguete = ((HappyMeal) menuSeleccionado).getJuguete();
        if(juguete.equals("Chico")) {
            ventana.cbJugueteComplemento.setSelectedIndex(0);
        }else {
            ventana.cbJugueteComplemento.setSelectedIndex(1);
        }
    }

}
