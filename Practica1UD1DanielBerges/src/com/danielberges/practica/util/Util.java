package com.danielberges.practica.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Clase Util
 * By Daniel Berges 08/11/21
 */
public class Util {

    /**
     * Método que muestra el mensaje de error
     * @param mensaje Variable String
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Métdo que muestra el mensaje de confirmación
     * @param mensaje Variable String
     * @param titulo Variable String
     * @return int
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null, mensaje, titulo, JOptionPane.YES_NO_OPTION);
    }

    /**
     * Método que crea el selector del fichero
     * @param rutaDefecto Variable File
     * @param tipoArchivos Variable String
     * @param extension Variable String
     * @return JFileChooser
     */
    public static JFileChooser crearSelectorFichero(File rutaDefecto, String tipoArchivos, String extension) {
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto != null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension != null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos, extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }

}
