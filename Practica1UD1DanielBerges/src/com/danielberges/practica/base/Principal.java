package com.danielberges.practica.base;

import com.danielberges.practica.gui.MenusControlador;
import com.danielberges.practica.gui.MenusModelo;
import com.danielberges.practica.gui.Ventana;

/**
 * Clase Principal
 * By Daniel Berges 08/11/21
 */
public class Principal {

    /**
     * Creo un objeto de la clase Ventana
     * Creo un objeto de la clase Modelo
     * Creo un objeto de la clase MenusControlador mandando los objetos como parámetros al constructor
     */
    public static void main(String[] args) {
        Ventana ventana = new Ventana();
        MenusModelo modelo = new MenusModelo();
        MenusControlador controlador = new MenusControlador(ventana, modelo);
    }

}
