package com.danielberges.practica.base;

import java.time.LocalDate;

/**
 * Clase HappyMeal
 * By Daniel Berges 08/11/21
 */
public class HappyMeal extends Menu {

    private String juguete;

    public HappyMeal() {
        super();
    }

    /**
     * Constructor de la clase HappyMeal
     * @param fechaPedido La fecha del pedido
     * @param numeroPedido El numero del pedido
     * @param hamburguesa El tipo de hamburguesa
     * @param patatas El tipo de patatas
     * @param bebida El tipo de bebida
     * @param detalles Los detalles del pedido
     * @param juguete El tipo de juguete
     */
    public HappyMeal(LocalDate fechaPedido, int numeroPedido, String hamburguesa, String patatas, String bebida, String detalles, String juguete) {
        super(fechaPedido, numeroPedido, hamburguesa, patatas, bebida, detalles);
        this.juguete = juguete;
    }

    public String getJuguete() {
        return juguete;
    }

    public void setJuguete(String juguete) {
        this.juguete = juguete;
    }

    /**
     * @return toString()
     */
    @Override
    public String toString() {
        return "Menu{" +
                "fechaPedido=" + getFechaPedido() +
                "numeroPedido=" + getNumeroPedido() +
                "hamburguesa=" + getHamburguesa() +
                "patatas=" + getPatatas() +
                "bebida=" + getBebida() +
                "detalles=" + getDetalles() +
                "juguete=" + getJuguete() +
                '}';
    }

}
