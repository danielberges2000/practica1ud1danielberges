package com.danielberges.practica.base;

import java.time.LocalDate;

/**
 * Clase Menu
 * By Daniel Berges 08/11/21
 */
public class Menu {

    private LocalDate fechaPedido;
    private int numeroPedido;
    private String hamburguesa;
    private String patatas;
    private String bebida;
    private String detalles;

    public Menu() {
    }

    /**
     * @param fechaPedido La fecha del pedido
     * @param numeroPedido El numero del pedido
     * @param hamburguesa El tipo de hamburguesa
     * @param patatas El tipo de patatas
     * @param bebida El tipo de bebida
     * @param detalles Los detalles del pedido
     */
    public Menu(LocalDate fechaPedido, int numeroPedido, String hamburguesa, String patatas, String bebida, String detalles) {
        this.fechaPedido = fechaPedido;
        this.numeroPedido = numeroPedido;
        this.hamburguesa = hamburguesa;
        this.patatas = patatas;
        this.bebida = bebida;
        this.detalles = detalles;
    }

    public LocalDate getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(LocalDate fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public int getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(int numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public String getHamburguesa() {
        return hamburguesa;
    }

    public void setHamburguesa(String hamburguesa) {
        this.hamburguesa = hamburguesa;
    }

    public String getPatatas() {
        return patatas;
    }

    public void setPatatas(String patatas) {
        this.patatas = patatas;
    }

    public String getBebida() {
        return bebida;
    }

    public void setBebida(String bebida) {
        this.bebida = bebida;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

}
