package com.danielberges.practica.base;

import java.time.LocalDate;

/**
 * Clase McAitana
 * By Daniel Berges 08/11/21
 */
public class McAitna extends Menu {

    private String complemento;

    public McAitna() {
        super();
    }

    /**
     * @param fechaPedido La fecha del pedido
     * @param numeroPedido El numero del pedido
     * @param hamburguesa El tipo de hamburguesa
     * @param patatas El tipo de patatas
     * @param bebida El tipo de bebida
     * @param detalles Los detalles del pedido
     * @param complemento El tipo de complemento
     */
    public McAitna(LocalDate fechaPedido, int numeroPedido, String hamburguesa, String patatas, String bebida, String detalles, String complemento) {
        super(fechaPedido, numeroPedido, hamburguesa, patatas, bebida, detalles);
        this.complemento = complemento;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    /**
     * @return toString()
     */
    @Override
    public String toString() {
        return "Menu{" +
                "fechaPedido=" + getFechaPedido() +
                "numeroPedido=" + getNumeroPedido() +
                "hamburguesa=" + getHamburguesa() +
                "patatas=" + getPatatas() +
                "bebida=" + getBebida() +
                "detalles=" + getDetalles() +
                "complemento=" + getComplemento() +
                '}';
    }
}
